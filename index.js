const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
//https://kb.objectrocket.com/mongo-db/how-to-setup-a-nodejs-app-with-mongodb-using-mongoose-227
const app = express();

const Mensa = require('./mensa'); //get the mensa moongse 

app.use(cors());
app.use(bodyParser.json());


//https://www.section.io/engineering-education/nodejs-mongoosejs-mongodb/
//Connect to  MongoDB 
const mongo = mongoose.connect('mongodb://localhost:27017/viralmensa', {useUnifiedTopology: true, useNewUrlParser: true});
mongo.then(() => {
    console.log('mongo connected');
}).catch((err) => {
    console.log('err', err);
});
const connection = mongoose.connection;

connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});



app.get("/", (req,res) => {
    res.json({ message: "Welcome to  viralMensa!  if you see this then welcome! " }); //testing

});





// find all Mensen -> return all informattion from https://openmensa.org/api/v2/canteens
app.get('/mensen', (req,res)=>{
    Mensa.Mensa.find((err, mensen) => {
        if (err) {
            console.log(err)
        } else {
            res.json(mensen);
        }
    });
});


// post and  update mensen 
app.post('/mensen', (req,res)=>{
    let newList = new Mensa.Mensa(
        req.body 
    );
    newList.save().then((listDoc) =>{
        res.send(listDoc);
    });
});


// find Mensa with ID
app.get('/mensa/:id', (req,res)=>{
    Mensa.Mensa.find( { id: req.params.id }, (err, mensa) => {
        if (err)
            console.log(err)
        else
            res.json(mensa);
    });
});




// set port, listen for requests
const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}.`);
});




